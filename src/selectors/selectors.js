import { createSelector } from 'reselect';

const selectorGroups = (groups) => groups.filter((item) => item.group_type === 'Some Group');
