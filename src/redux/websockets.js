import io from 'socket.io-client';
import { actionTypes, uri } from '../constants/api';

const allowedOrigins = 'http://localhost:3008/';
const socket = io(uri,
  {
    origins: allowedOrigins,
    transportOptions: {
      polling: {
        extraHeaders: {
          'Access-Control-Allow-Origin': allowedOrigins,
          'Access-Control-Allow-Credentials': 'true',
        },
      },
    },
  });

export const init = (store) => {
  Object.keys(actionTypes).forEach((type) => socket.on(type, (payload) => store.dispatch({ type, payload })));
};

export const emit = (type, payload) => {
  return socket.emit(type, payload);
};

