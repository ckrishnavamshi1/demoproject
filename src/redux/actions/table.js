export const GET_TABLE_DATA = "GET_TABLE_DATA";
import { actionTypes } from '../../constants/api';

export const getTableData = (dispatch, getState, { emit }) => {
  dispatch({ type: actionTypes.subscribe_tables });
  emit(actionTypes.subscribe_tables);
};
