import { GET_TABLE_DATA } from '../actions/table';

const initialState = [];

export const tableDataReducer = (state = initialState, { type, tableData }) => {
  switch (type) {
    case GET_TABLE_DATA:

      return [...state, ...tableData];

    default:
      return state;
  }
};
