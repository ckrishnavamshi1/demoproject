import { GET_POSTS } from '../actions/posts';

const initialState = [];

export const postReducer = (state = initialState, { type, posts }) => {
  switch (type) {
    case GET_POSTS:
      return [...posts];

    default:
      return state;
  }
};
