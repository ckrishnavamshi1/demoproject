import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import { tableDataReducer } from './reducers/table';
import { postReducer } from './reducers/posts';

export default combineReducers({
  routing: routerReducer,
  tableData: tableDataReducer,
  postsData: postReducer,
});
