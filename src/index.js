import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, withRouter } from 'react-router-dom';
import store from './redux/store';

import { AppComponents } from './containers/App/App';

const AppComponent = withRouter(props => <AppComponents {...props}/>);

ReactDOM.render((
  <Provider store={store}>
    <BrowserRouter>
      <AppComponent/>
    </BrowserRouter>
  </Provider>
), document.getElementById('app'));
