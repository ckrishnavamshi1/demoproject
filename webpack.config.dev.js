/* eslint-disable */

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    mode: 'development',
    devtool: 'eval-source-map',
    entry: [
        'babel-polyfill',
        './src/index.js'
    ],
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/preset-env', '@babel/preset-react']
                }
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader: "css-loader",
                        },
                        {
                            loader: "less-loader",
                            options: {
                                includePaths: ["./less/", "./public/css/"],
                            }
                        },
                    ]
                })
            }
        ],
    },
    plugins: [
        new webpack.ProvidePlugin({
            React: 'react'
        }),
        new ExtractTextPlugin({
            filename: 'style.css',
            allChunks: true,
        }),
    ],
    devServer: {
        host: 'localhost',
        disableHostCheck: true,
        port: 3008,
        contentBase: __dirname + '/dist',
        inline: true,
        hot: false,
        historyApiFallback: true
    },
};
